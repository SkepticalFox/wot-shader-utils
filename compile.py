﻿''' SkepticalFox 2019 '''

import os
import shutil
from data import get_shader_list, get_shader_info



try:
    shutil.rmtree('shaders')
except:pass




def write_property(f, data):
    f.write('{type} {name}\n<\n'.format(**data))
    for it in data['params']:
        f.write('\t{type} {name} = {value};\n'.format(**it))
    f.write('>')
    if data.get('default') is not None:
        f.write(' = {default}'.format(data))
    f.write(';\n\n\n')



def build(fx_path, info):
    with open(fx_path, 'w') as f:
        for pname, pvalue in info.items():
            data = {
                'name': pname,
                'params': [
                    {
                        'type': 'string',
                        'name': 'UIName',
                        'value': '"%s"' % pname
                    }
                ]
            }

            if pvalue['type'] == 'Texture':
                data['type'] = 'texture'

            elif pvalue['type'] == 'Int':
                data['type'] = 'int'
                if pvalue.get('default'):
                    data['default'] = pvalue['default']

            elif pvalue['type'] == 'Float':
                data['type'] = 'float'
                if pvalue.get('default'):
                    data['default'] = pvalue['default']

            elif pvalue['type'] == 'Bool':
                data['type'] = 'bool'
                if pvalue.get('default'):
                    data['default'] = 'true' if pvalue['default'] else 'false'

            elif pvalue['type'] == 'Vector4':
                data['type'] = 'float4'

            if pvalue.get('descr'):
                data['params'].append({
                    'type': 'string',
                    'name': 'UIDesc',
                    'value': '"%s"' % pvalue['descr']
                })

            write_property(f, data)



for fx_path in get_shader_list():
    path = os.path.dirname(fx_path)
    try:
        os.makedirs(path)
    except:pass
    info = get_shader_info(fx_path)
    build(fx_path, info)
