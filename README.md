# WoT shader utils #

Набор утилит для работы с шейдерами из игры World of Tanks

### Поддержка и разработка ###

Поддержка и разработка проекта осуществляется в [официальной теме проекта](https://kr.cm/f/t/45446/) на форумах Korean Random.

### Разработчики ###
[SkepticalFox](https://koreanrandom.com/forum/profile/16296-skepticalfox/)
