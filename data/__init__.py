﻿import json
import os


CWD = os.path.dirname(__file__)



with open(os.path.join(CWD, 'common.json'), 'r') as f:
    common = json.load(f)



with open(os.path.join(CWD, 'shaders.json'), 'r') as f:
    shaders = json.load(f)



def get_shader_list():
    return shaders.keys()



def get_shader_info(fx_name):
    info = shaders[fx_name]
    for key in info.keys():
        if key in common:
            info[key] = common[key]
    return info
